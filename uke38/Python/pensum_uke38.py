# Oppsummering fra sist
'''
sikkerhetsklarert = False
while True:
    navn = input("Skriv inn det hemmelige passordet: ")
    if navn == 'Børge':
        print('Børge? Du slipper aaaaaldri over denne broen!')
        break
    if navn == 'Rappo':
        if input('Rappo hvem?') == 'reven Rappo':
            sikkerhetsklarert = True
            break

print(f'Ble du sikkerhetsklarert? {sikkerhetsklarert}')
'''
'''
Dagens mål: gå igjennom funksjoner:
- inception - funksjoner kaller funksjoner
- retur fra funksjoner
- skop - lokale variable, samt overskygging
- globale variable (fy)

Hvorfor funksjoner?
- Enklere kode: Enklere å lese og forstå (mer logisk)
- Gjenbruk av kode: Slipper å skrive samme kode flere ganger
- Bedre testing: Testing og debugging er enklere med funksjoner (kan teste en og en funksjon)
- Raskere utvikling: Kode som trengs i flere deler av systemet kan brukes flere ganger
- Bedre støtte for samarbeid: Kan fordele funksjoner på flere programmerere 

'''

# Første linje: 'funksjonshode' - husk parenteser og kolon!
def funksjonsnavn(innparameter1, innparameter2): # 1 og 41 blir her parametre
    # kodeblokk med ting en må gjøre for å komme til svaret
    svar = innparameter1 + innparameter2 # Bare et eksempel, altså
    return svar # valgfritt om en returnerer noe spesifikt (men viktig!)

print(funksjonsnavn(1, 3))

# Et kjapt eksempel:
def gange(tall1, tall2):
    return tall1 * tall2

print(f'2*3 = {gange(2,3)}')

def barekompaaetnavn():
    print("her er jeg")

print(barekompaaetnavn())


# Dagens plan:

# Flere funksjoner:
# Sjekk ut flere_funksjoner.py

# Snakk litt om top-down, Børge! Dette med oppdeling av noe stort i mindre deler.
# Start med den der svarte boksen med én pil inn og én pil ut. 

# skop: sjekk ut skop.py
# - legg merke til at bruken av samme variabelnavn
# - legg merke til at funksjonen kjenner til navn
# - legg merke til at kode utenfor en funksjon ikke kjenner til en inni!
# Vis ppt-siden med skoping, ppt s.22

# Oppgave:
# Åpne oppgave_funksjoner.py


# Globale variable:
# Sjekk ut global_variabl.py
# ppt s.33 elns.

# Biblioteker
# Se matte_bibliotek.py
# Se random_stuff.py

# Oppgave:
# Se oppgave_funksjoner.py

# Hva er en modul _egentlig_ - jo, bare en fil!
# Se min_modul.py
#import min_modul
#min_modul.min_funksjon("hei på deg")

# Et bibliotek består av pakker som består av moduler. 

# Et stort eksempel: romertall_3.py

# nøkkelord

# (Utenfor pensum til eksamen, men fryktelig lurt. (tupler er pensum dog))
# Hva hvis jeg trenger å ende inn et ukjent antall parametre?
# Se ukjent_antall_parametre.py