import time
import psutil

# Vi kan bruke modulen time til å måle hvor lang tid noe tar!
start = time.time()

mem1 = psutil.Process().memory_info().rss
# Følgende kode lager bare en range - som gir ut tall når du ber om det
x = range(50000000)

# Når en lager en liste så lager den alle elementene med én gang
# x = list(range(50000000))
mem2 = psutil.Process().memory_info().rss

print("tid:",time.time() - start)
print("mem:",mem2-mem1)