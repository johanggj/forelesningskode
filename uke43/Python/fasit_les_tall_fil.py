filnavn = input("Oppgi navn på fila: ")
f = open(filnavn,"r") # Åpner fila for lesing

for linje in f: # Enda en måte å lese inn, trenger ikke readline()
    tall = int(linje) # Oversetter streng til heltall
    print(f'{tall} i tredje er {tall**3}.')
f.close()
